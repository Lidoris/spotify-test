package org.spotify.tests.services;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spotify.tests.configs.EnvironmentConfigs;
import org.spotify.tests.model.*;

import java.util.List;
import java.util.Optional;

import static org.spotify.tests.utils.Utils.logResponse;

public class SpotifyClient {
    private static final Logger log = LoggerFactory.getLogger(SpotifyClient.class);
    private final RequestSpecification requestSpecification;

    public SpotifyClient() {
        this.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(EnvironmentConfigs.getProperty("spotify.api.host"))
                .addHeader("Authorization", "Bearer " + AuthClient.getToken())
                .build();
    }

    public Artist getArtistById(String id) {
        String endpoint = "artists/" + id;
        log.info("Getting artist by id: {}", id);
        return getEntityOptional(endpoint, Artist.class)
                .orElseThrow(() -> new IllegalStateException(String.format("No artist with id %s", id)));
    }

    public List<Track> getTopTracks(String artistId) {
        String endpoint = "artists/" + artistId + "/top-tracks?country=us";
        log.info("Getting top-tracks of artist {}", artistId);
        return getEntityOptional(endpoint, Tracks.class)
                .orElseThrow(() -> new IllegalStateException(String.format("No top-tracks for %s", artistId))).getTracks();
    }

    public void addPlaylist(String name) {
        String endpoint = "users/" + EnvironmentConfigs.getProperty("spotify.client.id") + "/playlists";
        log.info("Adding a playlist {}", name);
        String body = String.format("{\"name\": \"%s\", \"public\": true}", name);
        Response response = RestAssured.with().spec(requestSpecification).body(body).post(endpoint);
        logResponse(response);
    }

    public List<Playlist> getPlaylists() {
        String endpoint = "users/" + EnvironmentConfigs.getProperty("spotify.client.id") + "/playlists";
        log.info("Getting user playlists");
        return getEntityOptional(endpoint, Playlists.class).orElse(new Playlists()).getItems();
    }

    private <T> Optional<T> getEntityOptional(String endpoint, Class<T> type) {
        Response response = RestAssured.with().spec(requestSpecification).get(endpoint);
        logResponse(response);
        return response.getStatusCode() != 200 ?
                Optional.empty() :
                Optional.of(new Gson().fromJson(response.getBody().asString(), type));
    }
}