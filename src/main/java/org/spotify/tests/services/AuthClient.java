package org.spotify.tests.services;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spotify.tests.configs.EnvironmentConfigs;
import org.spotify.tests.model.Token;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;

public class AuthClient {

    private static final Logger log = LoggerFactory.getLogger(AuthClient.class);
    private static String token;

    public static void authorize() {
        log.info("Authorizing to use Spotify API");
        String encodedData = null;
        try {
            encodedData = DatatypeConverter.printBase64Binary((EnvironmentConfigs.getProperty("spotify.client.id")
                    + ":" + EnvironmentConfigs.getProperty("spotify.client.secret")).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Could not encode credentials");
        }
        Response response = RestAssured.with()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Authorization", "Basic " + encodedData)
                .body("grant_type=client_credentials")
                .post("https://accounts.spotify.com/api/token");
        if (response.getStatusCode() != 200) {
            throw new IllegalStateException("Authorization token not provided: " + response.getBody().print());
        }
        token = new Gson().fromJson(response.getBody().asString(), Token.class).getAccessToken();
    }

    public static String getToken() {
        return token;
    }
}