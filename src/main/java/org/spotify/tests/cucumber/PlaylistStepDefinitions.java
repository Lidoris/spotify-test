package org.spotify.tests.cucumber;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Feature;
import net.thucydides.core.annotations.Steps;
import org.spotify.tests.constants.SessionVariable;
import org.spotify.tests.model.Playlist;
import org.spotify.tests.steps.SpotifySteps;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.spotify.tests.utils.Utils.getFromStorage;

@Feature
public class PlaylistStepDefinitions {

    @Steps
    private SpotifySteps spotifySteps;

    @When("^I try to create a new playlist (.*)$")
    public void createPlaylist(String name) {
        Serenity.getCurrentSession().put(SessionVariable.PLAYLIST, name);
        spotifySteps.addPlaylist(name);
    }

    @Then("^playlist is not created$")
    public void verifyPlaylistAbsence() {
        String name = getFromStorage(SessionVariable.PLAYLIST, String.class);
        List<Playlist> playlists = spotifySteps.getPlaylists();
        if (playlists != null) {
            assertThat(playlists).as("Playlist '" + name + "' should not be created")
                    .noneMatch(playlist -> name.equals(playlist.getName()));
        }
    }
}