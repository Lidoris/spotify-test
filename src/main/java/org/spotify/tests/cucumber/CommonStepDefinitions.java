package org.spotify.tests.cucumber;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Feature;
import org.spotify.tests.constants.SessionVariable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.spotify.tests.utils.Utils.getFromStorage;

@Feature
public class CommonStepDefinitions {

    @Then("^request is forbidden due to special rights needed$")
    public void checkForbidden() {
        int code = getFromStorage(SessionVariable.RESPONSE_CODE, Integer.class);
        assertThat(code).as("Request should be forbidden").isEqualTo(403);
    }
}