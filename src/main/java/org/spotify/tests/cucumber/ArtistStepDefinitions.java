package org.spotify.tests.cucumber;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Feature;
import net.thucydides.core.annotations.Steps;
import org.spotify.tests.constants.ArtistData;
import org.spotify.tests.constants.SessionVariable;
import org.spotify.tests.model.Artist;
import org.spotify.tests.model.Track;
import org.spotify.tests.steps.SpotifySteps;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.spotify.tests.utils.Utils.getFromStorage;

@Feature
public class ArtistStepDefinitions {

    @Steps
    private SpotifySteps spotifySteps;

    @When("^I get artist (.*)$")
    public void getArtist(String name) {
        String id = ArtistData.getArtistDataByName(name).getId();
        Serenity.getCurrentSession().put(SessionVariable.ARTIST, spotifySteps.getArtist(id));
    }

    @Then("^artist genres contain (.*)$")
    public void verifyPresenceOfGenres(String genres) {
        String[] expectedGenres = genres.split(",");
        List<String> actualGenres = getFromStorage(SessionVariable.ARTIST, Artist.class).getGenres();
        assertThat(actualGenres).as("Artist genres should match").contains(expectedGenres);
    }

    @Then("^artist genres do not contain (.*)$")
    public void verifyAbsenceOfGenres(String genres) {
        String[] unexpectedGenres = genres.split(",");
        List<String> actualGenres = getFromStorage(SessionVariable.ARTIST, Artist.class).getGenres();
        assertThat(actualGenres).as("Artist genres should not contain wrong genres")
                .doesNotContain(unexpectedGenres);
    }

    @Then("^artist popularity is (.*)$")
    public void verifyPopularity(int score) {
        int actualScore = getFromStorage(SessionVariable.ARTIST, Artist.class).getPopularity();
        assertThat(actualScore).as("Artist popularity should be correct").isEqualTo(score);
    }

    @When("^I get top-tracks of artist (.*)$")
    public void getTopTracks(String name) {
        String id = ArtistData.getArtistDataByName(name).getId();
        Serenity.getCurrentSession().put(SessionVariable.TOP_TRACKS,
                spotifySteps.getTopTracks(id)
                        .stream()
                        .map(Track::getName)
                        .toArray(String[]::new));
    }

    @Then("^artist top-tracks contain (.*)$")
    public void verifyTopTracks(String tracks) {
        String[] expectedTracks = tracks.split(",");
        String[] actualTracks = getFromStorage(SessionVariable.TOP_TRACKS, String[].class);
        assertThat(actualTracks).as("Artist top-tracks should match").contains(expectedTracks);
    }

    @Then("^artist top-tracks do not contain (.*)$")
    public void verifyAbsenceOfTracks(String tracks) {
        String[] unexpectedTracks = tracks.split(",");
        String[] actualTracks = getFromStorage(SessionVariable.TOP_TRACKS, String[].class);
        assertThat(actualTracks).as("Artist top-tracks should not contain wrong tracks")
                .doesNotContain(unexpectedTracks);
    }
}