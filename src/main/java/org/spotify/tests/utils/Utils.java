package org.spotify.tests.utils;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spotify.tests.constants.SessionVariable;

public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static <T> T getFromStorage(Object key, Class<T> type) {
        Object value = Serenity.getCurrentSession().get(key);
        if (value == null) {
            throw new IllegalStateException(String.format("Unable to retrieve value from storage by key %s. Value needs to be put to storage before it can be accessed", key));
        }
        if (value.getClass().equals(type)) {
            return ((T) value);
        }
        throw new IllegalStateException(String.format("Type of object retrieved by key %s was not %s", key, type));
    }

    public static void logResponse(Response response) {
        log.info("Response: {}", response.getStatusCode());
        Serenity.getCurrentSession().put(SessionVariable.RESPONSE_CODE, response.getStatusCode());
    }
}
