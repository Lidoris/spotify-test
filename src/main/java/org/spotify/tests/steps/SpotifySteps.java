package org.spotify.tests.steps;

import net.thucydides.core.annotations.Step;
import org.spotify.tests.model.Artist;
import org.spotify.tests.model.Playlist;
import org.spotify.tests.model.Track;
import org.spotify.tests.services.SpotifyClient;

import java.util.List;

public class SpotifySteps {

    private SpotifyClient spotifyClient = new SpotifyClient();

    @Step("Get artist {0}")
    public Artist getArtist(String artistId) {
        return spotifyClient.getArtistById(artistId);
    }

    @Step("Get top-tracks of artist {0}")
    public List<Track> getTopTracks(String artistId) {
        return spotifyClient.getTopTracks(artistId);
    }

    @Step("Add playlist {0}")
    public void addPlaylist(String name) {
        spotifyClient.addPlaylist(name);
    }

    @Step("Get playlists")
    public List<Playlist> getPlaylists() {
        return spotifyClient.getPlaylists();
    }
}