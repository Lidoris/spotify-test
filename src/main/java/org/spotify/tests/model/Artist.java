package org.spotify.tests.model;

import java.util.List;

public class Artist {
    private String name;
    private int popularity;
    private List<String> genres;

    public List<String> getGenres() {
        return genres;
    }

    public int getPopularity() {
        return popularity;
    }
}
