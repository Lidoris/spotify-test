package org.spotify.tests.model;

import com.google.gson.annotations.SerializedName;

public class Token {

    @SerializedName(value = "access_token", alternate = "accessToken")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }
}
