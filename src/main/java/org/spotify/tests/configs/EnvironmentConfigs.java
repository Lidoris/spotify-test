package org.spotify.tests.configs;

import net.serenitybdd.core.environment.ConfiguredEnvironment;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

public class EnvironmentConfigs {

    private static final EnvironmentVariables environmentVariables = ConfiguredEnvironment.getEnvironmentVariables();

    public static String getProperty(String property) {
        return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(property);
    }
}