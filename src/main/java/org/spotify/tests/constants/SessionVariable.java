package org.spotify.tests.constants;

public enum SessionVariable {
    ARTIST,
    TOP_TRACKS,
    PLAYLIST,
    RESPONSE_CODE;
}
