package org.spotify.tests.constants;

import java.util.NoSuchElementException;
import java.util.stream.Stream;

public enum ArtistData {
    LANA_DEL_REY("Lana Del Rey", "00FQb4jTyendYWaN8pK0wa"),
    SIA("Sia", "5WUlDfRSoLAfcVSX1WnrxN"),
    COLDPLAY("Coldplay", "4gzpq5DPGxSnKTe4SA8HAU"),
    MUSE("Muse", "12Chz98pHFMPJEknJQMWvI"),
    CEZA("Ceza", "28Qbi9jTj2eej21P2mImZI"),
    LADY_GAGA("Lady Gaga", "1HY2Jd0NmPuamShAr6KMms"),
    BEYONCE("Beyonce", "6vWDO969PvNqNYHIOW5v0m");

    private String name;
    private String id;

    ArtistData(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public static ArtistData getArtistDataByName(String name) {
        return Stream.of(ArtistData.values()).filter(data -> data.name.equals(name))
                .findFirst().orElseThrow(() -> new NoSuchElementException("The artist is not recognized"));
    }

    public String getId() {
        return id;
    }
}
