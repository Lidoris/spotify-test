Feature: Artists

  Scenario Outline: Artist genres match
    When I get artist <artist>
    Then artist genres contain <genres>
    And artist genres do not contain <wrongGenres>
    Examples:
      | artist       | genres             | wrongGenres |
      | Lana Del Rey | pop,art pop        | rock        |
      | Sia          | australian pop     | country     |
      | Coldplay     | permanent wave,pop | azeri rap   |
      | Muse         | rock,modern rock   | hip hop     |

  Scenario Outline: Artist popularity
    When I get artist <artist>
    Then artist popularity is <score>
    Examples:
      | artist    | score |
      | Ceza      | 63    |
      | Coldplay  | 88    |
      | Lady Gaga | 86    |
      | Beyonce   | 85    |
