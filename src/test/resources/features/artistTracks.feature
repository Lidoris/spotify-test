Feature: Artist tracks

  Scenario Outline: Artist top-tracks
    When I get top-tracks of artist <artist>
    Then artist top-tracks contain <tracks>
    And artist top-tracks do not contain <wrongTracks>
    Examples:
      | artist       | tracks              | wrongTracks                |
      | Lana Del Rey | Young And Beautiful | Take it easy               |
      | Sia          | Del Mar,Chandelier  | Remember                   |
      | Coldplay     | Yellow,Paradise     | Psycho                     |
      | Muse         | Uprising,Psycho     | Era,What a wonderful world |
