Feature: User playlists

  Scenario: Forbidden playlist creation
    When I try to create a new playlist Music
    Then request is forbidden due to special rights needed
    And playlist is not created
