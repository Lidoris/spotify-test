package org.spotify.tests.cucumber;

import io.cucumber.junit.CucumberOptions;

@CucumberOptions(
        features = {"src/test/resources/features/"},
        glue = {"org.spotify.tests.cucumber"})
public class LocalRunner extends BaseRunner {
}
