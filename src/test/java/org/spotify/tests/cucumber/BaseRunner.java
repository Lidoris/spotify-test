package org.spotify.tests.cucumber;

import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.spotify.tests.services.AuthClient;

import java.util.concurrent.atomic.AtomicBoolean;

@RunWith(CucumberWithSerenity.class)
public class BaseRunner {

    private static final AtomicBoolean beforeAllFlag = new AtomicBoolean(true);
    private static final Object lock = new Object();

    @BeforeClass
    public static void beforeClass() {
        synchronized (lock) {
            if (beforeAllFlag.get()) {
                AuthClient.authorize();
                beforeAllFlag.set(false);
            }
        }
    }
}
