## Spotify API tests

### Run configurations
* In order to run the tests via IDEA the following runner class can be used: LocalRunner\
    (in this case the tests will run in one thread one by one)

* The tests are configured to run in parallel with the command: mvn clean verify\
    (in this case class runners will be generated for each feature file)

### Creation of new tests
* The following architecture should be followed:\
    Feature files -> Cucumber step definitions -> Serenity steps -> Services
* Feature files and step definitions should be devided depending on the endpoints they are related to
* Artist specific data which should not be displayed in feature files can be stored in enums inside the code (e.g. ArtistData)

### CI/CD pipeline
* Test results are stored in each pipeline in Job Artifacts